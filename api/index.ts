import { getEnvironment } from './client'

export interface ContentType {
    id: string
    name: string,
    fields: any[]
}

export enum Routes {
  CONTENT_TYPES = '/api/contentful/content-types'
}

export async function getContentTypes():Promise<ContentType[]>{
    const env = await getEnvironment();
    const contentTypesResponse = await env.getContentTypes();
    return contentTypesResponse.items.map( ({sys, name, fields}) => ({
        id: sys.id,
        name,
        fields
    }));
 }