
const {createClient} = require('contentful-management');
const cache: any = {};

export async function init() {

    const { managementToken, activeSpaceId, activeEnvironmentId} = require('./.contentfulrc.json')
	
	const client = await createClient({ accessToken: managementToken });
	const space = await client.getSpace(activeSpaceId);
	const env = await space.getEnvironment(activeEnvironmentId);

	cache.client = client;
	cache.space = space;
	cache.env = env;

	console.info(`contentful using space-id=${activeSpaceId}, environment-id = ${activeEnvironmentId}`);

	return {
		client,
		space,
		env
	}
}

export async function getSpace() {
	if(cache.space) { return cache.space; }	
	const { space } = await init();
	return space
}


export async function getEnvironment() {
	console.info('getEnvironment: cache', Object.keys(cache));
	if(cache.env) { 
		console.info('env (from cache):', cache.env);		
		return cache.env;
	}	

	const { env } = await init();
	console.info('env: ', env);
	return env
}