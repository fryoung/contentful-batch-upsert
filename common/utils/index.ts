import { DEFAULT_LOCALE } from '../constants'
import {ContentTypes} from '../types'
import { getContentTypes } from '../../api';
export * from './contentful'

/**
 * Utilities to assist with Contentful Management tasks
 */

/**
 * Return iso timestamp
 * YYYYMMDD-HHMMSS
 */
export function getTimestamp():string {
   const d = new Date();
   return `${d.getFullYear()}${d.getMonth()}${d.getDate()}-${d.getHours()}${d.getMinutes()}${d.getSeconds()}`
 }

export function getExtension(fileName):string | null{
    const extPos = fileName.lastIndexOf('.');
    let ext = fileName.substring(extPos);
    return ext.length ? ext : null
 }

 export function getContentType(fileName:string, {contentType}:{contentType?:string} = {}):string {
    if(contentType){
        return contentType;
    }

    const hasExtension:boolean = !!getExtension(fileName);
    const ext = getExtension(fileName);
    switch(ext) {
        case "png":
            return ContentTypes.PNG;
        default: 
            // defaults to JPG here if no filename extension
            return ContentTypes.JPG;
    }
 }

 function getFileNameFromURL(url: string):string | null {

    let pathname:string = new URL(url).pathname.trim();
    let fileNamePosition:number = pathname.lastIndexOf('/');

    // check for trailing slash
    if(fileNamePosition === pathname.length - 1) {
        pathname = pathname.substring(0, pathname.length - 1);
        fileNamePosition = pathname.lastIndexOf('/');
    }

    const parsedFileName:string = pathname.substring(fileNamePosition);
    return parsedFileName.length ? parsedFileName : null;
 }

