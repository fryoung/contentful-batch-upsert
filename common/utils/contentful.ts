
import {Environment} from 'contentful-management/typings/environment'
import { DEFAULT_LOCALE } from '../constants'
import {getTimestamp} from './'

function createAssetId(asset) {
    const locale = DEFAULT_LOCALE;
    const fileName = asset.fields.file[locale].fileName;
    const fileNameClean = fileName.split('.')[0].toLowerCase().replace(/[#$%^&*!@\s]/g);
    const ts = getTimestamp();
    return `${fileNameClean}-${ts}`;
}


function createFilePayload({contentType, fileName, url, locale = DEFAULT_LOCALE}) {
    return {
        [locale]: {
            contentType,
            fileName,
            upload: url
        }
    }
}

/**
 * Method to take an array of asset objects, then call Contentful api to actually create those assets
 * Array of assets will be returned
 * @param env
 * @param assets 
 */
export function createAssets(env, assetList): Promise<any> {
    const requests = [];
    assetList.forEach( item => {
        const assetId = createAssetId(item)
        requests.push(
            env.createAssetWithId(assetId, item).then( asset => {
                asset.process();
            })
        );
    });
    return Promise.all(requests);
}

/**
 * 
 * @param data Maps CSV record to Contentful Entry fieds
 * @param entryMap 
 * @param locale 
 */
 function createEntryFields(env, data, entryMap, locale = DEFAULT_LOCALE) {

    const assetFields = entryMap.fields.find( field => {
        return ['image','images', 'asset'].includes(field.field);
    });

    if(assetFields) {}
    else {}

    return new Promise((resolve, reject)=>{
        const fields = entryMap.fields.reduce(async (acc, field) => {
            if(['image','images', 'asset'].includes(field.field)){
                const list = data[field.field] ? data[field.field].split(',') : []
                const assets = list.map( item => ({
                    fields: {
                        file: createFilePayload( {contentType:'' ,fileName: '', url: item} ),
                        title: {
                            [locale]: `image for ${item ? item.id : 'unknown'}`
                        }
                    }
                }))
                if(assets) {
                    acc[field.field] = await createAssets(env, assets)
                }
            }
            else {
                acc[field.field] = {
                    [locale] : data[field.column]
                };
            }
            return acc;
        }, {});

        resolve(fields)
    })


}

// data.fields.reduce( (acc,field) => {
//     acc[field.name] = {
//         [locale]: field.value 
//     }
//     return acc;
// }, {})

/**
 * Function to build contentful entries
 * @param env -- reference to contentful environment
 * @param csvRows array of records create from CSV rows
 * @param entryMap 
 */
export async function buildEntries(env: Environment, csvRows: any[], entryMap) {

    if(!Array.isArray(csvRows)) {
        console.error('csvRows needs to be an array');
        return null;
    }
    const entries = csvRows.reduce(async (acc, row) => {
        // env, data, entryMap, locale = DEFAULT_LOCALE)
        const fields = await createEntryFields(env, row, entryMap)
        acc.push({fields});
        return acc;
    }, []);

    return entries;
}

export async function previewEntries(csvRows: any[], entryMap) {
    if(!Array.isArray(csvRows)) {
        console.error('csvRows needs to be an array');
        return null;
    }
    const entries = csvRows.reduce((acc, row) => {
        const fields = createEntryFields(null, row, entryMap)
        acc.push({fields});
        return acc;
    }, []);
    return entries;
}