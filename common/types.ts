export enum ContentTypes {
    TEXT = "text/plain",
    JSON = "application/json",
    JPG = "image/jpeg",
    PNG = "image/png",
    GIF = "image/gif",
}