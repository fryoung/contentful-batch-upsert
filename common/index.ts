export const steps = [
    {
        label: 'Upload spreadsheet',
        instructions: 'Upload spreadsheet. Column headings should be in the first row.'
    },
    {
        label: 'Select ContentType',
        instructions: 'Choose the ContentType that you wish to create entries for'
    },
    {
        label: 'Preview',
        instructions: 'Preview ContentType entries that will be created in final step'        
    },
    {
        label: 'Create Entries',
        instructions: 'Create entries in contentful'
    }    
]