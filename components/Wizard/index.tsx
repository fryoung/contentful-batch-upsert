import React, { useState, useReducer, Component } from 'react'
import fetch from 'isomorphic-fetch'
import styled, { StyledComponent } from 'styled-components';
import csv from 'csvtojson'

import Upload from '../Upload'
import CSVTable from '../CSVTable'
import StepRenderer from '../StepRenderer'
import ContentTypeSelector from '../ContentTypeSelector'
import Processing from '../Processing'
import reducer from './reducer'

import { buildEntries, previewEntries } from '../../common/utils'
import { steps } from '../../common'
import { Routes } from '../../api'


enum Step {
  UPLOAD_SPREADSHEET,
  CHOOSE_CONTENT_TYPE,
  CHOOSE_COLUMNS,
  CREATE_CONTENTFUL_ENTRIES,
}

const Container = styled.div`
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: '#e5e5e5';
  color: #999;
  transition: border .24s ease-in-out;
`;

const acceptedFileTypes = 'text/*,.csv'

interface CSVFile {
    raw: string
    data: any | null
}

interface WizardState {
  upload: CSVFile | null
  entries: any[] | null
  currentStep: number
  contentTypeItems: any[] | null
}

export default class Wizard extends Component<{}, WizardState> {

  constructor(props) {
    super(props)
    this.state = {
      entries: null,
      upload: {
        raw: null,
        data: null
      },
      currentStep: 0,
      contentTypeItems: null
    }
  }

  async componentDidMount() {
    const response = await fetch(Routes.CONTENT_TYPES);
    const {result: contentTypeItems} = await response.json();
    this.setState({
      contentTypeItems
    })
  }

  renderStepContent(stepIndex: number) {
    const { upload, contentTypeItems, entries } = this.state
    switch (stepIndex) {
      case Step.UPLOAD_SPREADSHEET:
        return <Upload 
        accept={acceptedFileTypes} 
        onLoad={this.onUploadComplete} 
      />;
      case Step.CHOOSE_CONTENT_TYPE:
        return <ContentTypeSelector
          onFieldChangeHandler={this.onFieldChangeHandler}
          onPreviewEntriesClick={this.onPreviewEntriesClick}
          contentTypeItems={contentTypeItems}
          record={upload.data[0]}
        />
      case Step.CREATE_CONTENTFUL_ENTRIES:
        return <>
            <pre>
              { JSON.stringify(entries, null, 4) }
            </pre>
            <Processing />
        </>
      default: 
          return null
    }

    return null;
  }

  onUploadComplete = async (uploadResult: string) => {

    console.log('onUploadComplete', uploadResult);
    const data = await csv({noheader: false}).fromString(uploadResult);

    this.setState({
      upload: {
        raw: uploadResult,
        data
      },
      currentStep: Step.CHOOSE_CONTENT_TYPE
    });
  }

  nextStep = () => {
    let step = this.state.currentStep
    if(this.state.currentStep + 1 < steps.length){
       this.setState({
         currentStep: (step + 1)
       })
    }
  }

  onPreviewEntriesClick = async ( payload ) => {
    const entries = await previewEntries(this.state.upload.data as any[], payload);
    this.setState({
      entries
    }, () => {
      this.nextStep();
    })
  }
  
  onFieldChangeHandler = (e) => {
   console.log('onFieldChangeHandler', e);
  }  

  render() {
    const { currentStep } = this.state
    return (
      <Container>
        <StepRenderer steps={steps} currentStep={currentStep} />
        {this.renderStepContent(currentStep)}
      </Container>
    )
  }

}
