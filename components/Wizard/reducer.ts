export default function reducer(state, action) {
    switch (action.type) {
      case 'currentStep':
        return { currentStep: action.payload as number };
      default:
        throw new Error();
    }
  }