import React from 'react'
import styled from 'styled-components'
import ContentfulLogo from '../Icons/ContentfulLogo'

const Box = styled.div`
   display: flex;
   flex-direction: column;
   padding: 10px;
   ul {
       list-style-type: none;
       margin: 0px;
       padding: 0px;
       li {
           padding: 5px;
       }
   }
`

function ContentfulInfo({ user, space, environment}){
    return <Box>
    <ContentfulLogo />    
    <ul>
     <li>User: {user}</li>
     <li>Space: {space}</li>
     <li>Environment: {environment}</li>
    </ul>
    </Box>
}

export default ContentfulInfo