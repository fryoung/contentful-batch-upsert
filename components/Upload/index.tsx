import React, {useCallback} from 'react'
import {useDropzone, DropzoneState} from 'react-dropzone'
import styled, { StyledComponent } from 'styled-components';

const getColor = (props: DropzoneState) => {
    if (props.isDragAccept) {
        return '#00e676';
    }
    if (props.isDragReject) {
        return '#ff1744';
    }
    if (props.isDragActive) {
        return '#2196f3';
    }
    return '#eeeeee';
  }

interface ContainerProps {
  props: any
}

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  border-width: 2px;
  border-radius: 2px;
  border-color: ${(props: DropzoneState) => getColor(props)};
  border-style: dashed;
  background-color: #fafafa;
  color: #bdbdbd;
  outline: none;
  transition: border .24s ease-in-out;
`;

interface UploadFile extends File {
    path?: string,    
}

interface DropZoneProps {
  accept: string,
  onLoad: (content: any) => any
}

function Upload({accept, onLoad}: DropZoneProps) {
  
  const onDrop = useCallback((acceptedFiles) => {
    acceptedFiles.forEach((file: UploadFile) => {
      const reader = new FileReader();
      reader.onabort = () => console.log('file reading was aborted')
      reader.onerror = () => console.log('file reading has failed')
      reader.onload = ()=> {
        onLoad(reader.result);
      }
      reader.readAsText(file);
    })    
  }, []);

  const {getRootProps, getInputProps, acceptedFiles, rejectedFiles} = useDropzone({
      multiple: false,
      onDrop,
      accept
  })

const acceptedFilesItems = acceptedFiles.map((file: UploadFile, index: number, array: UploadFile[]) => (
     <li key={file.path}>
       {file.path} - {file.size} bytes
     </li>
   ));

  const rejectedFilesItems = rejectedFiles.map((file: UploadFile) => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));  

  return (
    // @ts-ignore
    <Container {...getRootProps()}>
      <input {...getInputProps()} />
      <p>Drag spreadsheet(s) here</p>
      <em>(Only {accept} files will be accepted)</em> 
    </Container>
  )
}

{/* <aside>
<h4>Accepted files</h4>
<ul>
  {acceptedFilesItems}
</ul>
</aside>  */}
   
export default Upload;