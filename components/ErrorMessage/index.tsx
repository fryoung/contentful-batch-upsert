import styled from 'styled-components'

const ErrorMessageWrapper = styled.div`
    padding: 30px;
    margin: 30px;
    border: 1px solid #e5e5e5;
    color: red;
`


export default function ErrorMessage({children}) {
   return <ErrorMessageWrapper>
       {children}
    </ErrorMessageWrapper>
} 