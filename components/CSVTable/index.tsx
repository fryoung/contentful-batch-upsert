import React, {Component} from 'react'
import csv from 'csvtojson'
import Table from '@contentful/forma-36-react-components/dist/components/Table/Table'
import TableHead from '@contentful/forma-36-react-components/dist/components/Table/TableHead'
import TableBody from '@contentful/forma-36-react-components/dist/components/Table/TableBody'
import TableRow from '@contentful/forma-36-react-components/dist/components/Table/TableRow'
import TableCell from '@contentful/forma-36-react-components/dist/components/Table/TableCell'

import ColumnEditor from './ColumnEditor'

interface CSVTableProps {
    csvStr: string
    showBody: boolean
    showHeader: boolean
}

interface CSVTableState {
    header?: any[]
    rows: any[]
}

export default class CSVTable extends Component <CSVTableProps, CSVTableState>{

    static defaultProps = {
        showBody: false,
        showHeader: true
    }

    constructor(props) {
        super(props)
        this.state = {
            rows: []
        };
    }

    async componentDidMount(){
        const {csvStr} = this.props
        const result = await csv({
            noheader: false
        })
        .fromString(csvStr);
        const header:string[] = Object.keys(result[0]);
        this.setState({
            rows: result,
            header
        });
    }

    render(){
        const { rows, header } = this.state
        const { showBody, showHeader } = this.props
        return (
          <>
          {showHeader && rows.length && <ColumnEditor columns={header} /> }
          {showBody &&          
          <Table>
                <TableBody>
                {rows.map( (cells, idx) => 
                    <TableRow key={`tablerow-${idx}`}>
                        {Object.keys(cells).map( fieldName => 
                        <TableCell key={fieldName}>
                            {cells[fieldName]}
                        </TableCell>
                        )}
                    </TableRow>
                )}
                </TableBody>
         </Table>
         }         
         </>
         )
    }
}