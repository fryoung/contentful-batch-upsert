import React, { ChangeEvent, useState, useReducer } from 'react'
import Checkbox from '@contentful/forma-36-react-components/dist/components/Checkbox'
import Button from '@contentful/forma-36-react-components/dist/components/Button'
import Form from '@contentful/forma-36-react-components/dist/components/Form/Form'
import styled from 'styled-components'

import {columnEditorReducer} from './reducer'

const ColumnName = styled.input`
    border: 1px solid #ddd;
    background: white;
    padding: 6px;
    font-size: 10pt;
`

const FormContainer = styled.div`
margin-bottom: 20px;
`

const EditorCell = styled.li`
  display: block;
  align-items: center;
`

const initialState: any = {

}

export default function ColumnEditor({columns}: {columns: any[]}){

    const [state, dispatch] = useReducer(initialState, columnEditorReducer);
    const mappedCols:any[] = columns.map( col => ({id: col, checked: true}))  
    const [cols, setValue] = useState(mappedCols)

    function onChange(e: ChangeEvent) {
      const input:HTMLInputElement = e.currentTarget as HTMLInputElement
      console.log(input.value);      
    }

    function onCheck(e: React.ChangeEvent) {
      const input:HTMLInputElement = e.currentTarget as HTMLInputElement
      console.log(input.checked);
    }

    function applyChanges() {
      console.log('Apply Changes');
    }

    return (
      <>
        <FormContainer>
        <Form>       
          <ul>
          {
            mappedCols.map( col => (
              <EditorCell>
                  <Checkbox onChange={onCheck} checked={col.checked} labelText={''} value={col.id} />
                  <ColumnName onChange={onChange} value={col.id} />
              </EditorCell>
            ))
          }
          </ul>
        </Form>
        </FormContainer>
        <Button style={{display: 'block'}} onClick={applyChanges}> Apply Changes</Button>
        </>    
    )
}