
import React, {Fragment, useState} from 'react'
import SelectField from '@contentful/forma-36-react-components/dist/components/SelectField/SelectField';
import Option from '@contentful/forma-36-react-components/dist/components/Select/Option';
import HelpText from '@contentful/forma-36-react-components/dist/components/HelpText';
import Subheading from '@contentful/forma-36-react-components/dist/components/Typography/Subheading';
import Button from '@contentful/forma-36-react-components/dist/components/Button';
import ErrorMessage from '../ErrorMessage'

import {ContentType} from '../../api'
import styled from 'styled-components'


const Container = styled.div`
    font-size: 10pt;
    [class*='SelectField__'] {
        max-width: 300px;
        margin-bottom: 20px;
    }
`

const Table = styled.table`
   border: 1px solid #e5e5e5;
   margin: 15px 0px;
   th {
       padding: 10px;
       background-color: #e5e5e5;
       min-width: 200px;
   }
   td {
       padding: 6px;
       border-bottom: 1px solid #e5e5e5
   }
   tr:last-child td {
      border-bottom: none
   }   
`

const ColumnTitle = styled.div`
    font-weight: bold;
`

function getMatchedColumn(fieldName: string, record: Record, returnContent: boolean = false){

    let match = (()=>{
        if(record.hasOwnProperty(fieldName)){
            return returnContent ? record[fieldName] : fieldName;
        }
        else if(record.hasOwnProperty(fieldName.toLowerCase())) {
            return returnContent ? record[fieldName.toLowerCase()]: fieldName.toLowerCase();
        }
        else {
            return Object.keys(record).reduce((r, key) => {
                if(key.toLowerCase() === fieldName.toLowerCase()) {
                    return returnContent ? record[key] : key;
                }
                return r;
            }, null);
        }
    })();

    return match ? match : returnContent ? '' : '--';
}

interface ContentTypeSelectorProps {
    contentTypeItems: ContentType[]
    onFieldChangeHandler: (e: any) => void
    onPreviewEntriesClick: (payload: any) => void
    record: Record
}

interface Record {
  [key: string]: any
}

const ContentTypeSelector = ({
    contentTypeItems, 
    onFieldChangeHandler,
    onPreviewEntriesClick,
    record}:ContentTypeSelectorProps) => {

    if(!contentTypeItems) {
        return <ErrorMessage>
            Error trying to get Contentful ContentTypes
        </ErrorMessage>
    }

    const defaultContentTypeId = contentTypeItems.length ? contentTypeItems[0].id : null;
    const [selectedContentTypeId, setContentType] = useState(defaultContentTypeId)
    const selectedContentType = contentTypeItems.find( cType => cType.id === selectedContentTypeId);

    const ContentTypeFields = ({fields}):JSX.Element => {
        return (
            <>
            <Subheading>
            ContentType:<span style={{color: '#3072be'}}>{selectedContentTypeId}</span>  to CSV Column mapping
            </Subheading>
            <Table>
                <thead>
                    <tr>
                        <th>ContentType Fields</th>
                        <th>Matched Spreadsheet Column</th>
                    </tr>
                </thead>
                <tbody>
                {fields 
                    ? fields.map( field => 
                        <tr key={`${field.name.toLowerCase()}`}>
                            <td>{ field.name }</td>
                            <td>
                                <ColumnTitle>{ getMatchedColumn(field.id, record) }</ColumnTitle>
                                <div>{ getMatchedColumn(field.id, record, true) }</div>
                            </td>                            
                        </tr>) 
                    : null
                }
                </tbody>
            </Table>
            <HelpText>
                '--' indicates that no matching CSV column was found for ContentType field. 
                <br />Unmatched CSV columns will not be used to create Contentful Entries
            </HelpText>
            <Button 
                onClick={handlePreviewEntriesClick}
                style={{marginTop: '20px'}} 
                buttonType="primary">Everything look okay? Click to Preview Entries</Button>
            </>
        )
    }

    function handlePreviewEntriesClick(){

        const fields = selectedContentType.fields.reduce( (acc, field) => {
            const name = getMatchedColumn(field.id, record)
            if(name && name !== '--') {
                acc.push({column: name, field: field.id});
            }
            return acc
        }, []);

        const payload = {
            contentType: selectedContentTypeId,
            fields
        }
        onPreviewEntriesClick(payload);
    }

    function onChange(e){
        const contentType = e.currentTarget.value
        const match = contentTypeItems.find( o => o.id === contentType)
        setContentType(match ? match.id : null)
        onFieldChangeHandler(contentType);
    }

    return (
    <Container>
          <SelectField
            key={'contentType'}
            data-testid='cf-ui-select--contentType'
            id='contentType'
            name='contentType'
            labelText='Content Type'
            value={selectedContentTypeId}
            onChange={onChange}
        >
        {contentTypeItems.map((item, idx) => 
           <Option key={`contentType${idx}-${item.id}`} value={item.id}>{item.name}</Option>
        )}
        </SelectField>
       {selectedContentType && <ContentTypeFields fields={selectedContentType.fields} />}
       {!selectedContentTypeId && <div>Could not access Content Types</div>}
    </Container>
    )
};

export default ContentTypeSelector