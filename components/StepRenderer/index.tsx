import React, { ReactElement } from 'react'
import styled from 'styled-components'
import classnames from 'classnames'

const List = styled.ul`
   display: flex;
   list-style-type: none;
   margin: 0px;
   padding: 0px;
   li {
       padding: 10px;
       padding: 10px 20px;
       color:#444;
       font-weight: bold;
       background-color: #e5e5e5;
       transition: all 0.5s;
       
       &.selected {
           background-color: #151515;
           color: #fff
       }
       &:first-child {
          border-top-left-radius: 20px;
          border-bottom-left-radius: 20px;
       }
       &:last-child {
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
     }       
   }
`

const SubTitle = styled.div`
    color: #888;
    padding: 10px;
    margin-top: 20px;    
    margin-bottom: 20px;
    font-size: 12pt;
`

export default function Steps({steps, currentStep = 0}: {steps: any, currentStep?: number}) {
    const selectedStep: any =  steps[currentStep]
    return (
        <>
        <List>
            {steps.map( (step, idx) => {
                return <li 
                    key={`step-${idx}`}
                    className={classnames({'selected': idx === currentStep})}> 
                    {(idx + 1)}. {step.label} 
                </li>
            })}
        </List>
        <SubTitle>{selectedStep.instructions}</SubTitle>
        </>
    )
}