import styled from 'styled-components'
import { bounce } from '../../styles/keyframes';
const Container = styled.div`
   padding: 20px;
   span {
       content: '';
       width: 10px;
       height: 10px;
       border-radius: 50%;
       background-color: #aaa;
   }
   span:nth-child(1) {
      animation: ${bounce} .6s infinite;       
   }
   span:nth-child(2) {
      animation: ${bounce} .6s infinite;
   }
   span:nth-child(3) {
      animation: ${bounce} .6s infinite;
   }   
`

export default function Processing(){
    return <Container>
        <span />
        <span />
        <span />
    </Container>
}