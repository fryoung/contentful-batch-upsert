
import { getContentTypes } from '../../../api'


export default async (req:any, res: any) => {
    const contentTypes = await getContentTypes();
    res.status(200).json({
        result: contentTypes
    });
};