import {createWriteStream} from "fs";
export default async (req, res) => {
    // for the pipe to work, we need to disable "bodyParser" (see below)
    req.pipe(createWriteStream("./tmp/foo.jpg"));
    res.statusCode = 200;
    res.end();
};
export const config = {
    api: {
        bodyParser: false,
    },
};