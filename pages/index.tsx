import { useState } from 'react'
import { NextPage } from 'next'
import Wizard from '../components/Wizard'

import styled, { StyledComponent } from 'styled-components';
import ContentfulInfo from '../components/ContentfulInfo'
import '@contentful/forma-36-react-components/dist/styles.css';
import '@contentful/forma-36-fcss/dist/styles.css';

const Section = styled.section`
   padding: 30px;
   font-family: Helvetica Neue, Helvetica,Arial;
`

const contentfulProps = {
    user: 'Frank Young',
    space: '<contentful-space-id>',
    environment: '<contentful-environment-id>'
}

const Home: NextPage<{ userAgent: string }> = ({ userAgent }) =>     
    <Section>
        <h1>Batch Contentful Entry (POC)</h1>
        <ContentfulInfo {...contentfulProps} />
        <Wizard />
    </Section>;

Home.getInitialProps = async ({ req }) => {
    const userAgent: string | undefined = req ? req.headers['user-agent'] : navigator.userAgent;
    return { userAgent };
};


export default Home;