 const ErrorPage = err => {
    return <div className='error'>
        <h1>uh, something went wrong...</h1>
        {
           JSON.stringify(err, null, 2)
        }
    </div>
}

export default ErrorPage