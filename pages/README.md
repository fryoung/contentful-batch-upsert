## Batch Creation of Entries

This POC application allows a user to upload a CSV file and create contentful entries from the rows in the CSV. First Row is expected to have column headings in it.

#### Sample spreadsheet:

| FirstName| LastName | Address|
|----------| -------- | ----------|
| Linus   | Torvalds   | 1234 Main Street |
| Tim     | Berners-Lee | 4321 Page Street |


### Installation

```
npm install
```

### Development

```
npm run dev
```


---

### 3rd Party Contentful Integrations

Zapier has a plugin that allows you to create Entries in Contentful from Rows in Google Sheets

https://zapier.com/apps/contentful/integrations/google-sheets